#include "../Headers/InputConnection.hpp"

InputConnection::InputConnection(int layerIndex) {
    float randomFloat;

    this->value = 1.0;

    if (layerIndex == 0) {
        this->weight = 1;
    } else {
        randomFloat = FLOAT_RAND();
        this->weight = randomFloat * sqrt(2.0 / getLayerWidth(layerIndex - 1));
    }
}

InputConnection::~InputConnection(){}

void InputConnection::setValue(float value) {
    this->value = value;
}

float InputConnection::getValue() {
    return this->value;
}

float InputConnection::getWeight() {
    return this->weight;
}

float InputConnection::getWeighedValue() {
    return (this->value * this->weight);
}