#include "../Headers/Layer.hpp"

Layer::Layer(int layerIndex) {
    for (int i = 0; i < getLayerWidth(layerIndex); i++) {
        layer.push_back(Neuron(layerIndex));
    }
}

Layer::~Layer() {}

void Layer::connectNeurons(Neuron* neuronToConnectWith) {
    for (int i = 0; i < this->layer.size(); i++) {
        this->layer.at(i).createOutputConnection(neuronToConnectWith->createInputConnection());
    }
}

int Layer::getWidth() {
    return this->layer.size();
}

Neuron* Layer::getNeuronAddress(int neuronIndex) {
    return &this->layer.at(neuronIndex);
}

void Layer::setNeuronValue(int neuronIndex, float value) {
    this->layer.at(neuronIndex).setValue(value);
}

void Layer::pushValues() {
    for (int i = 0; i < this->layer.size(); i++) {
        this->layer.at(i).pushValue();
    }
    
}