#include "../Headers/NeuralNetwork.hpp"

NeuralNetwork::NeuralNetwork() {
    for (int i = 0; i <= networkSettings.nbrOfBodyLayer + 1; i++) {
        network.push_back(Layer(i));
    }
}

NeuralNetwork::~NeuralNetwork() {}

void NeuralNetwork::connectNeurons() {
    for (int i = this->network.size() - 2; i >= 0; i--) {
        for (int j = 0; j < this->network.at(i + 1).getWidth(); j++) {
            this->network.at(i).connectNeurons(this->network.at(i + 1).getNeuronAddress(j));
        }
    }
}

int NeuralNetwork::getNumberOfLayer() {
    return this->network.size();
}

Layer* NeuralNetwork::getLayerAddress(int layerIndex) {
    return &this->network.at(layerIndex);
}

int NeuralNetwork::setInputVectorData(std::vector<std::vector<float>>* inputVector) {
    if (inputVector->at(0).size() != this->network.at(0).getWidth()) {
        std::cout<<"Wrong input layer width :\nInput vector width = "\
        <<inputVector->at(0).size()<<"\nInput layer width = "<<\
        this->network.at(0).getWidth();
        return ERROR;
    }
    this->inputVector = inputVector;
    return NO_ERROR;
}
    
float NeuralNetwork::trainNetwork() {
    for (int i = 0; i < NBR_OF_LOOP; i++) {
        for (int j = 0; j < inputVector->size(); j++) {
            for (int k = 0; k < this->network.at(0).getWidth(); k++) {
                this->network.at(0).setNeuronValue(k, this->inputVector->at(j).at(k));
            }
            for (int k = 0; k < this->network.size(); k++) {
                this->network.at(k).pushValues();
            }
        }
    }
    return 1.0;
}