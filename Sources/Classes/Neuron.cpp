#include "../Headers/Neuron.hpp"

Neuron::Neuron(int layerIndex) {
    this->bias = 0;
    this->layerIndex = layerIndex;
}

Neuron::~Neuron() {}

int Neuron::getNumberofInputConnection() {
    return this->inputConnections.size();
}

int Neuron::getNumberofOutputConnection() {
    return this->outputConnections.size();
}

InputConnection* Neuron::createInputConnection() {
    this->inputConnections.push_back(InputConnection(this->layerIndex));
    return &this->inputConnections.back();
}

void Neuron::createOutputConnection(InputConnection* neuronInputToConnectWith) {
    this->outputConnections.push_back(OutputConnection(neuronInputToConnectWith));
}

InputConnection* Neuron::getInputConnectionAddress(int inputConnectionsIndex) {
    std::list<InputConnection>::iterator iterator = this->inputConnections.begin();

    for (int i = 0; i < inputConnectionsIndex; i++) {
        iterator++;
    }
    return &(*iterator);
}

OutputConnection* Neuron::getOutputConnectionAddress(int outputConnectionsIndex) {
    std::list<OutputConnection>::iterator iterator = this->outputConnections.begin();

    for (int i = 0; i < outputConnectionsIndex; i++) {
        iterator++;
    }
    return &(*iterator);
}

void Neuron::setValue(float value) {
    this->value = value;
}

void Neuron::pushValue() {
    std::list<OutputConnection>::iterator iterator = this->outputConnections.begin();

    this->getValueFromInputs();
    for (int i = 0; i < this->outputConnections.size(); i++) {
        iterator++;
        (*iterator).pushValue(this->value);
    }
}

void Neuron::getValueFromInputs() {
    float value;
    std::list<InputConnection>::iterator iterator = this->inputConnections.begin();

    for (int i = 0; i < this->inputConnections.size(); i++) {
        iterator++;
        value += (*iterator).getWeighedValue();
    }
    value += this->bias;
    this->value = value;
}