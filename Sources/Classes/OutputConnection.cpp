#include "../Headers/OutputConnection.hpp"

OutputConnection::OutputConnection(InputConnection* inputConnection)
{
    this->inputConnection = inputConnection;
}

OutputConnection::~OutputConnection()
{
}

void OutputConnection::pushValue(float value) {
    this->inputConnection->setValue(value);
}

InputConnection* OutputConnection::getInputConnectionAddress() {
    return this->inputConnection;
}