#include "../Headers/Settings.hpp"

NetworkSettings networkSettings;

int getLayerWidth(int layerIndex) {
    if (layerIndex == 0) {
        return networkSettings.inputLayerWidth;
    } else if (layerIndex <=  networkSettings.nbrOfBodyLayer) {
        return networkSettings.bodyLayerWidth;
    } else {
        return networkSettings.outputLayerWidth;
    }
}