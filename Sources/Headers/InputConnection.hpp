#ifndef _INPUT_CONNECTION_
#define _INPUT_CONNECTION_

#include <cstdlib>
#include <cmath>
#include <iostream>

#include "./Settings.hpp"

class InputConnection
{
private:
    float value;
    float weight;
public:
    InputConnection(int layerWidth);
    ~InputConnection();

    void setValue(float value);
    float getValue();
    float getWeight();
    float getWeighedValue();
};

#endif