#ifndef _LAYER_
#define _LAYER_

#include <vector>

#include "./Neuron.hpp"

class Layer
{
private:
    std::vector<Neuron> layer;
public:
    Layer(int layerIndex);
    ~Layer();

    int getWidth();
    Neuron* getNeuronAddress(int neuronIndex);
    void connectNeurons(Neuron* neuronToConnectWith);
    void setNeuronValue(int neuronIndex, float value);
    void pushValues();
};

#endif