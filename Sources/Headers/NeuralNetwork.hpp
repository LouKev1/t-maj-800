#ifndef _NEURAL_NETWORK_
#define _NEURAL_NETWORK_

#include "./Layer.hpp"

class NeuralNetwork
{
private:
    std::vector<Layer> network;
    std::vector<std::vector<float>>* inputVector;
    std::vector<float> results;
public:
    NeuralNetwork();
    ~NeuralNetwork();

    void connectNeurons();
    int getNumberOfLayer();
    Layer* getLayerAddress(int layerIndex);
    int setInputVectorData(std::vector<std::vector<float>>* inputVector);
    float trainNetwork();
};

#endif
