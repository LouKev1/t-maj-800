#ifndef _NEURON_
#define _NEURON_

#include <iostream>
#include <list>

#include "./Settings.hpp"
#include "./InputConnection.hpp"
#include "./OutputConnection.hpp"

class Neuron
{
private:
    std::list<InputConnection> inputConnections;
    std::list<OutputConnection> outputConnections;
    float bias;
    int layerIndex;
    float value;
public:
    Neuron(int layerIndex);
    ~Neuron();

    InputConnection* getInputConnectionAddress(int inputConnectionsIndex);
    OutputConnection* getOutputConnectionAddress(int outputConnectionsIndex);
    int getNumberofInputConnection();
    int getNumberofOutputConnection();
    InputConnection* createInputConnection();
    void createOutputConnection(InputConnection* neuronInputToConnectWith);
    void setValue(float value);
    void getValueFromInputs();
    void pushValue();
};

#endif
