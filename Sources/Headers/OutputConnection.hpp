#ifndef _OUTPUT_CONNECTION_
#define _OUTPUT_CONNECTION_

#include "./InputConnection.hpp"

class OutputConnection
{
private:
    InputConnection* inputConnection;
public:
    OutputConnection(InputConnection* inputConnection);
    ~OutputConnection();

    void pushValue(float value);
    InputConnection* getInputConnectionAddress();
};

#endif