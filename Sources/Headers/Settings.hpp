#ifndef _SETTINGS_
#define _SETTINGS_

#define ERROR 1
#define NO_ERROR 0

#ifdef _TEST_
    #define NN_INPUT_WIDTH 5U
    #define NN_BODY_WIDTH 3U
    #define NN_OUTPUT_WIDTH 1U
    #define BODY_LAYER_NUMBER 4U
#else
    #define NN_INPUT_WIDTH 5U
    #define NN_BODY_WIDTH 4U
    #define NN_OUTPUT_WIDTH 1U
    #define BODY_LAYER_NUMBER 2U
    #define NBR_OF_LOOP 1000U
#endif

#define SIGMOID(VALUE) ((float)VALUE / (0.5 + abs((float)VALUE)) / 2) + 0.5
#define FLOAT_RAND() static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/WEIGHT_INIT_LIMIT))

#define WEIGHT_INIT_LIMIT 3U

typedef struct NetworkSettings {
    const int nbrOfBodyLayer = BODY_LAYER_NUMBER;
    const int inputLayerWidth = NN_INPUT_WIDTH;
    const int bodyLayerWidth = NN_BODY_WIDTH;
    const int outputLayerWidth = NN_OUTPUT_WIDTH;
}NetworkSettings;

extern NetworkSettings networkSettings;

int getLayerWidth(int layerIndex);

#endif