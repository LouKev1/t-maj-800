#include "Headers/Settings.hpp"
#include "Headers/NeuralNetwork.hpp"

int main(int argc, char const **argv)
{
    NeuralNetwork neuralNetwork;
    neuralNetwork.connectNeurons();
    return 0;
}
