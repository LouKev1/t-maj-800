#include <iostream>

#include "test.hpp"

void doTest(int &totalNbrOfFail, int &nbrOfFail, TestData function) {
    nbrOfFail = function.test();
    totalNbrOfFail += nbrOfFail;
    std::cout<<function.testName<<" : "<<nbrOfFail<<" Fail\n";
}

int main(int argc, char const *argv[])
{
    int totalNbrOfFail = 0;
    int nbrOfFail = 0;

    for (int i = 0; i < NBR_OF_TESTS; i++) {
        doTest(totalNbrOfFail, nbrOfFail, testList[i]);
    }

    std::cout<<"\nNombre total de Fail : "<<totalNbrOfFail<<"\n";
    return 0;
}
