#include "test.hpp"

TestData testList[NBR_OF_TESTS] = {
    FUNC_DEF(testCreationInputConnextion)
    FUNC_DEF(testCreationOutputConnextion)
    FUNC_DEF(testCreationNeuron)
    FUNC_DEF(testCreationLayer)
    FUNC_DEF(testCreationNeuralNetwork)
};