#ifndef _TEST_HEADER_
#define _TEST_HEADER_

#include "../Sources/Headers/InputConnection.hpp"
#include "../Sources/Headers/OutputConnection.hpp"
#include "../Sources/Headers/Neuron.hpp"
#include "../Sources/Headers/Layer.hpp"
#include "../Sources/Headers/NeuralNetwork.hpp"
#include "../Sources/Headers/Settings.hpp"

#define EPSILON 1.19209e-07

#define NBR_OF_TESTS 5

int testCreationInputConnextion();
int testCreationOutputConnextion();
int testCreationNeuron();
int testCreationLayer();
int testCreationNeuralNetwork();

#define FUNC_DEF(test) { test, #test },

typedef struct TestData{
    int(*test)() ;
    const char * testName;
}TestData;

extern TestData testList[NBR_OF_TESTS];

#endif