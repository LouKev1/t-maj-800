/* TEST de fonctionnement
 *
 * Ce test vise à savoir si :
 * 
 * - Les classes utilisées dans le neuronne s'initialisent bien.
 * - Le réseau de neuronne se construit comme prévus.
 * - Les neuronnes se connectent bien entre eux.
 * 
*/

#include "../test.hpp"

int testCreationInputConnextion() {
    int nbrOfFail = 0;
    InputConnection inputConnection(0);
    
    /* Test of the initial value of value */
    if (abs(inputConnection.getValue() - 1.0) >= EPSILON)
        nbrOfFail++;

    /* Test of the setValue function */
    inputConnection.setValue(4.2);
    if (abs(inputConnection.getValue() - 4.2) >= EPSILON)
        nbrOfFail++;
    return nbrOfFail;
}

int testCreationOutputConnextion() {
    int nbrOfFail = 0;
    InputConnection inputConnection(0);
    OutputConnection outputConnection(&inputConnection);
    
    /* Test of the pushValue function */
    outputConnection.pushValue(4.2);
    if (abs(inputConnection.getValue() - 4.2) >= EPSILON)
        nbrOfFail++;
    return nbrOfFail;
}

int testCreationNeuron() {
    int nbrOfFail = 0;
    Neuron neuron(0);

    /* Test of the inputConnection vector initialisation */
    if (neuron.getNumberofInputConnection() != 0)
        nbrOfFail++;

    /* Test of the outputConnection vector initialisation */
    if (neuron.getNumberofOutputConnection() != 0)
        nbrOfFail++;

    /* Test of the createInputConnection function */
    neuron.createInputConnection();
    if (neuron.getNumberofInputConnection() != 1)
        nbrOfFail++;

    /* Test of the createOutputConnection function */
    neuron.createOutputConnection(neuron.createInputConnection());
    if (neuron.getNumberofInputConnection() != 2)
        nbrOfFail++;
    if (neuron.getNumberofOutputConnection() != 1)
        nbrOfFail++;
    return nbrOfFail;
}

int testCreationLayer() {
    int nbrOfFail = 0;
    Layer layer(3);
    Neuron neuron(0);
    Neuron* pNeuron;

    /* Test of the layer vector initialisation */
    if (layer.getWidth() != 3)
        nbrOfFail++;

    /* Test of the getNeuronAddress function */
    pNeuron = layer.getNeuronAddress(0);
    for (int i = 1; i < 3; i++) {
        if (pNeuron == layer.getNeuronAddress(i))
            nbrOfFail++;
        pNeuron = layer.getNeuronAddress(i);
    }

    /* Test of the connectNeurons function */
    layer.connectNeurons(&neuron);
    if (neuron.getNumberofInputConnection() != 3)
        nbrOfFail++;

    /* Test of the getNumberofOutputConnection function */
    for (int i = 0; i < 3; i++) {
        if (layer.getNeuronAddress(i)->getNumberofOutputConnection() != 1)
            nbrOfFail++;
    }
    return nbrOfFail;
}

int testCreationNeuralNetwork() {
    int nbrOfFail = 0;
    NeuralNetwork network;

    /* Test of the initialisation of the layers */
    if (network.getNumberOfLayer() != 5)
        nbrOfFail++;

    /* Test of the initialisation of the first layer */
    if (network.getLayerAddress(0)->getWidth() != 5)
        nbrOfFail++;

    /* Test of the initialisation of the body layers */
    if (network.getLayerAddress(2)->getWidth() != 3)
        nbrOfFail++;

    /* Test of the initialisation of the last layer */
    if (network.getLayerAddress(4)->getWidth() != 1)
        nbrOfFail++;

    /* Test of the connectNeurons function | check that the neurons have the right number of connections */
    network.connectNeurons();
    if (network.getLayerAddress(0)->getNeuronAddress(0)->getNumberofInputConnection() != 0)
        nbrOfFail++;

    for (int i = 0; i < 5; i++) {
        if (network.getLayerAddress(0)->getNeuronAddress(i)->getNumberofOutputConnection() != 3)
            nbrOfFail++;
    }

    for (int i = 0; i < 3; i++) {
        if (network.getLayerAddress(i)->getNeuronAddress(0)->getNumberofOutputConnection() != 3)
            nbrOfFail++;
    }

    for (int i = 0; i < 3; i++) {
        if (network.getLayerAddress(3)->getNeuronAddress(i)->getNumberofOutputConnection() != 1)
            nbrOfFail++;
    }

    if (network.getLayerAddress(4)->getNeuronAddress(0)->getNumberofOutputConnection() != 0)
        nbrOfFail++;

    /* Test of the connectNeurons function | check that the neurons connections points at the right place */
    if (network.getLayerAddress(0)->getNeuronAddress(0)->getOutputConnectionAddress(0)->getInputConnectionAddress() != network.getLayerAddress(1)->getNeuronAddress(0)->getInputConnectionAddress(0))
        nbrOfFail++;
    if (network.getLayerAddress(0)->getNeuronAddress(4)->getOutputConnectionAddress(2)->getInputConnectionAddress() != network.getLayerAddress(1)->getNeuronAddress(2)->getInputConnectionAddress(4))
        nbrOfFail++;
    if (network.getLayerAddress(3)->getNeuronAddress(0)->getOutputConnectionAddress(0)->getInputConnectionAddress() != network.getLayerAddress(4)->getNeuronAddress(0)->getInputConnectionAddress(0))
        nbrOfFail++;
    if (network.getLayerAddress(3)->getNeuronAddress(2)->getOutputConnectionAddress(0)->getInputConnectionAddress() != network.getLayerAddress(4)->getNeuronAddress(0)->getInputConnectionAddress(2))
        nbrOfFail++;
    return nbrOfFail;
}
